////
////  APIManagar.swift
////  PracticalTatva
////
////  Created by SOTSYS129 on 11/01/21.
////  Copyright © 2021 MitalK. All rights reserved.
////
//
//import Foundation
//import Alamofire
//
//class ApiCallManager {
//    
//    
//    
//    //note : here can create common function on returning data  or dictornary in closer here I have used 3 API due time sortage
//    static let shared = ApiCallManager()
//    private init() {  }
//
//    func makeAPICall(with endPoint: String, method: HTTPMethod, params: Parameters?, encoding:  ParameterEncoding, headers: HTTPHeaders?,  completion: @escaping (Error?, UserList?,Int?) -> Void) {
//        
//        AF.request(endPoint, method: method, parameters: params, encoding: encoding , headers: headers)
//            
//            .response { (response) in
//               
//                if let error = response.error {
//                    print("Error making an API call. - \(error.localizedDescription)")
//                    completion(error, nil, nil)
//                    return
//                }
//                if let data = response.data {
//                    do {
//                        let model = try JSONDecoder().decode(UserList.self, from: data)
//                      
//                         do {
//                         let jsonResult = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? AnyObject
//                         
//                            print("jsonResult = \(String(describing: jsonResult))")
//                         
//                         }catch {
//                         print("post data error",error)
//                         }
//                        
//                        completion(nil, model, nil)
//                    } catch let error {
//                        guard let status = response.response?.statusCode else{return}
//                        if status == 500{
//                            print("internal server error")
//                        }
//                        print("error status code is:\(String(describing: response.response?.statusCode))")
//                        print("Error decoding - \(error.localizedDescription)")
//                        completion(error, nil, status)
//                    }
//                    return
//                }
//        }
//        
//    }
//    func makeAPICallGet(with endPoint: String, method: HTTPMethod, params: Parameters?, encoding:  ParameterEncoding, headers: HTTPHeaders?,  completion: @escaping (Error?, Location?,Int?) -> Void) {
//        
//        AF.request(endPoint, method: method, parameters: params, encoding: encoding , headers: headers)
//            
//            .response { (response) in
//               
//                if let error = response.error {
//                    print("Error making an API call. - \(error.localizedDescription)")
//                    completion(error, nil, nil)
//                    return
//                }
//                if let data = response.data {
//                    do {
//                        let model = try JSONDecoder().decode(Location.self, from: data)
//                      
//                         do {
//                         let jsonResult = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? AnyObject
//                         
//                            print("jsonResult = \(String(describing: jsonResult))")
//                         
//                         }catch {
//                         print("post data error",error)
//                         }
//                        
//                        completion(nil, model, nil)
//                    } catch let error {
//                        guard let status = response.response?.statusCode else{return}
//                        if status == 500{
//                            print("internal server error")
//                        }
//                        print("error status code is:\(String(describing: response.response?.statusCode))")
//                        print("Error decoding - \(error.localizedDescription)")
//                        completion(error, nil, status)
//                    }
//                    return
//                }
//        }
//        
//    }
//    func makeAPICallGet1(with endPoint: String, method: HTTPMethod, params: Parameters?, encoding:  ParameterEncoding, headers: HTTPHeaders?,  completion: @escaping (Error?, NearLocation?,Int?) -> Void) {
//        
//        AF.request(endPoint, method: method, parameters: params, encoding: encoding , headers: headers)
//            
//            .response { (response) in
//               
//                if let error = response.error {
//                    print("Error making an API call. - \(error.localizedDescription)")
//                    completion(error, nil, nil)
//                    return
//                }
//                if let data = response.data {
//                    do {
//                        let model = try JSONDecoder().decode(NearLocation.self, from: data)
//                      
//                         do {
//                         let jsonResult = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? AnyObject
//                         
//                            print("jsonResult = \(String(describing: jsonResult))")
//                         
//                         }catch {
//                         print("post data error",error)
//                         }
//                        
//                        completion(nil, model, nil)
//                    } catch let error {
//                        guard let status = response.response?.statusCode else{return}
//                        if status == 500{
//                            print("internal server error")
//                        }
//                        print("error status code is:\(String(describing: response.response?.statusCode))")
//                        print("Error decoding - \(error.localizedDescription)")
//                        completion(error, nil, status)
//                    }
//                    return
//                }
//        }
//        
//    }
//}
