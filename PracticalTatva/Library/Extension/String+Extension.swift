//
//  String+Extension.swift
//  PracticalTatva
//
//  Created by SOTSYS129 on 11/01/21.
//  Copyright © 2021 MitalK. All rights reserved.
//
import Foundation
extension String{
    
    var trim: String {
        return trimmingCharacters(in: .whitespacesAndNewlines)
    }
    var isNotEmpty : Bool{
        if self.trim.count > 0{
            return true
        }else{
            return false
        }
    }
}
