//
//  Service.swift
//  PracticalTatva
//
//  Created by SOTSYS129 on 11/01/21.
//  Copyright © 2021 MitalK. All rights reserved.
//

import UIKit
import CoreData

class Service{
    
    static let sharedInstance = Service()
    
    func getMovieList(page:Int = 0,complition:@escaping ((_ isSuccess :Bool, _ errorMessage:String?,_ data: Movies?) -> Void)){
        
        guard let url = URL.init(string: BASE_URL) else {return}
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
                guard let data = data else{
                    print(error.debugDescription)
                    return
                }
                do{
                    var movieData = [Results]()
                    let result = try JSONDecoder().decode(Movies.self, from: data)
                    movieData = result.results!
                    self.saveMovieInDB(data: movieData)
                    complition(true, nil, result)
                }catch let jsonError{
                    print(jsonError.localizedDescription)
                }
            
        }.resume()
    }
    func getMovieDetail(id:String,complition:@escaping ((_ isSuccess :Bool, _ errorMessage:String?,_ data: MovieDetails?) -> Void)){
        
            print("details",id)
            let str = "\(id)?api_key=14bc774791d9d20b3a138bb6e26e2579"
           guard let url = URL.init(string: DetailURL+str) else {return}
           
           URLSession.shared.dataTask(with: url) { data, response, error in
               
                   guard let data = data else{
                       print(error.debugDescription)
                       return
                   }
                   do{
                       let result = try JSONDecoder().decode(MovieDetails.self, from: data)
                       self.saveMovieDetailInDB(data: result)
                       complition(true, nil, result)
                   }catch let jsonError{
                       print(jsonError.localizedDescription)
                   }
               
           }.resume()
       }
    func saveMovieInDB(data : [Results]){
        
        for obj in data{
            let movie = MovieList(context: CoreDataManagar.context)
            guard let title = obj.title else {return}
            guard let date = obj.release_date else {return}
            guard let overview = obj.overview else {return}
            guard let imgUrl = obj.poster_path else {return}
            guard let dId = obj.id else {return}
            
            movie.detailId = "\(dId)"
            movie.title = title
            movie.date = date
            movie.overview = overview
            movie.imgUrl = imgUrl
            CoreDataManagar.saveContext()
        }
    }
    func deleteMovie(){
        
        let context = CoreDataManagar.context
        let delete = NSFetchRequest<NSFetchRequestResult>(entityName:"MovieList")
        let detReq = NSBatchDeleteRequest(fetchRequest:delete)
        
        do{
            try context.execute(detReq)
            try context.save()
        }catch{
            print("")
        }
    }
    
    //TO DO delete or update id is there in db
    func deleteMovieDetail(){
        
        let context = CoreDataManagar.context
        let delete = NSFetchRequest<NSFetchRequestResult>(entityName:"MovieDetail")
        let detReq = NSBatchDeleteRequest(fetchRequest:delete)
        
        do{
            try context.execute(detReq)
            try context.save()
        }catch{
            print("")
        }
    }
    
    func saveMovieDetailInDB(data : MovieDetails){
        
            let obj = MovieDetail(context: CoreDataManagar.context)
            guard let id = data.id else {return}
            let overview = data.overview ?? ""
            let genres = data.overview ?? "" ////data.genres TODO
            let duration = "\(String(describing: data.runtime ?? 0))Minutes"
            let releaseDate = data.release_date ?? ""
            let prodCompany = "\(String(describing: data.budget ?? 0))" //TODO
            let prodBudget = "\(String(describing: data.budget ?? 0))"
            let revenue = "\(String(describing: data.revenue ?? 0))"
            let lang = data.overview ?? "" //data.spoken_languages  //TODO
            if let belong = data.belongs_to_collection{
                 obj.posterPath = belong.poster_path ?? ""
                 obj.backPath = belong.backdrop_path ?? ""
            }else{
                obj.posterPath = ""
                obj.backPath = ""
            }
            let name = data.original_title ?? ""
             
             let tagline = data.tagline ?? ""
            
            obj.id = "\(id)"
            obj.overview = overview
            obj.genres = genres
            obj.duration = "\(duration)"
            obj.releaseDate = releaseDate
            obj.prodCompany = "\(prodCompany)"
            obj.prodBudget = "$\(prodBudget)"
            obj.revenue = "$\(revenue)"
            obj.lang = lang
            obj.name = name
            obj.tagline = tagline
            CoreDataManagar.saveContext()
        
//        TODO
        //        var prodCom = ""
        //        if data.production_companies!.count > 0{
        //            let company = data.production_companies!
        //            prodCom = company.map { ($0.name ?? "")}.joined(separator:",")
        //            }
        //        }
    }

    func getMovieListDB() -> [MovieList]?{
        
        let fetchRequest : NSFetchRequest<MovieList> = MovieList.fetchRequest()
        do{
            let data = try CoreDataManagar.context.fetch(fetchRequest)
            if let array = data as? [MovieList]{
                return array
            }
        }catch {
            print(error.localizedDescription)
        }
        return []
    }
    func getMovieDetailDB(id :String) -> MovieDetail?{
        
         let fetchRequest = NSFetchRequest<MovieDetail>(entityName: "MovieDetail")
         fetchRequest.predicate = NSPredicate(format: "id == %@", id)
        do{
            let data = try CoreDataManagar.context.fetch(fetchRequest)
            return data.first
            
        }catch {
            print(error.localizedDescription)
        }
        return nil
    }
}
    
