//
//  MovieDetailViewController.swift
//  PracticalTatva
//
//  Created by SOTSYS129 on 11/01/21.
//  Copyright © 2021 MitalK. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {

    let listViewModel = MovieListViewModel()
    
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var lblOverview: UILabel!
    @IBOutlet weak var lblGenres: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblReleaseDate: UILabel!
    @IBOutlet weak var lblProdComp: UILabel!
    @IBOutlet weak var lblProdBudget: UILabel!
    @IBOutlet weak var lblRevenue: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    var detailId :String?
    override func viewDidLoad() {
        super.viewDidLoad()

        if checkInternet{
            listViewModel.getMovieDetailFromServer(byid: detailId!)
        }else{
            listViewModel.getMovieDetailFromDB(id: detailId!)
        }
        listViewModel.refreshDetailData = { [weak self] in
            DispatchQueue.main.async {
                
                let detail = self!.listViewModel.movieDetail
                self!.lblOverview.text = detail.overview
                self!.lblGenres.text = detail.genres
                self!.lblDuration.text = detail.duration
                self!.lblReleaseDate.text = detail.releaseDate
                self!.lblProdComp.text = detail.prodCompany
                self!.lblProdBudget.text = detail.prodBudget
                self!.lblRevenue.text = detail.revenue
                self!.lblLanguage.text = detail.lang
                if let imageurl = detail.backPath{
                    let path = Constant.ImageBasePath + imageurl
                    self!.imgBack.sd_setImage(with: URL(string:path),placeholderImage:#imageLiteral(resourceName: "placeholder"))
                }
            }
        }
    }
    
    
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
