//
//  MovieListViewController.swift
//  PracticalTatva
//
//  Created by SOTSYS129 on 11/01/21.
//  Copyright © 2021 MitalK. All rights reserved.
//

import UIKit



class MovieListViewController: UIViewController {

    @IBOutlet weak var tblMovieList: UITableView!
    let listViewModel = MovieListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tblMovieList.register(UINib.init(nibName: MovieListTblCell.className, bundle: Bundle.main), forCellReuseIdentifier: MovieListTblCell.className)
        
        
        if checkInternet{
            listViewModel.getDataFromServer()
        }else{
            listViewModel.getDataFromDB()
        }
        listViewModel.refreshData = { [weak self] in
            DispatchQueue.main.async {
                self?.tblMovieList.reloadData()
            }
        }
    }
}
extension MovieListViewController: UITableViewDelegate,UITableViewDataSource{


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listViewModel.movieData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: MovieListTblCell.className, for: indexPath) as! MovieListTblCell
        cell.modelData = listViewModel.movieData[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let detail = storyboard.instantiateViewController(withIdentifier: MovieDetailViewController.className) as! MovieDetailViewController
            detail.detailId = listViewModel.movieData[indexPath.row].detailId
            self.navigationController?.pushViewController(detail, animated: true)
    }
}
